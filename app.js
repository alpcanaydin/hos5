/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */


/*
* Load load load!
*/

 // Load config module
 var config = require("./helpers/config");

// Load some dependencies for express framework
var express = require('express')
  , routes = require('./routes')
  , page = require('./routes/page')
  , http = require('http')
  , path = require('path')
  , uuid = require('node-uuid');

// Create express application
var app = express();

// Configure express
app.configure(function(){
  app.set('port', process.env.PORT || config.config.port);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('alpcanaydin'));
  app.use(express.session());
  app.use(app.router);
  app.use(require('less-middleware')({ src: __dirname + '/public' }));
  app.use(express.static(path.join(__dirname, 'public')));
});

// Set environment to development
app.configure('development', function(){
  app.use(express.errorHandler());
});

// Create server for node and start it given port
var server = http.createServer(app).listen(app.get('port'), function(){
  console.log("Hos5 server started on port " + app.get('port'));
});

// Load socket.io and assign it to io variable
var io = require('socket.io').listen(server);

io.configure(function() {
  io.set("transports", ["websocket", "xhr-polling"]);
  io.set("polling duration", 10);
  io.set("log level", 0);
  io.set('close timeout', 180);
  io.set('heartbeat timeout', 180);
});

// Load mongoose module and create connection for hos5 database
var mongoose = require("mongoose");
var db = mongoose.createConnection(config.config.mongodb.host, config.config.mongodb.database, config.config.mongodb.port);

// Load city model
require("./models/city");

// Load conversation model
require("./models/conversation");

// Load visitlog model
require("./models/visitlog");

//-------------------------------------------------------------------


/**
 * Socket.io Stuff
 */

// Let's some works here!
// Do your job soldier when the enemies attack us!
io.sockets.on('connection', function (client) {

  // Set null city var
  var city = null;

  // Set City model
  var City = db.model("City");

  // Set Conversation model
  var Conversation = db.model("Conversation");

  // Set VisitLog model
  var VisitLog = db.model("VisitLog");

  // Add this visit to database
  var visitlog = new VisitLog({
    clientID: client.id,
    ip: client.handshake.address
  });

  // Save this visit
  visitlog.save();

  // Stranger matching process function
  var matchProcess = function(data, first) {
    // Make integer city id
    city = parseInt(data.city);

    // Add client to current city
    client.join(city);

    // Check if it is first connection from client
    if (first === true) {
      // Update count of city
      cityCountUpdate(city, 1);
    }

    client.set("enabled", true, function() {
      // Notify the client for waiting for stranger
      client.emit('waitingForStranger');

      // Get all clients in city
      var clients = io.sockets.clients(city);

      var go = true;

      // If there are some user in this city
      if (clients.length > 0) {
        var i = 0;

        // Take of them randomly
        while (go == true) {
          clients = io.sockets.clients(city);

          // If list is finished, then finish the loop
          if (i == clients.length) {
            go = false;
          }

          // Get a stranger randomly
          matched = clients[Math.floor(Math.random()*clients.length)];

          // If this user enabled for chat and it is also not you finish the loop
          if ( matched.store.data.enabled == true && client.id != matched.id) {
            go = false;
          } else {
            matched = null;
          }

          i++;
        }
      }

      // If we matched with a stranger
      if (matched != null) {
        // Create unique room id
        var room = "chat" + uuid.v1();

        // Client joined to this room
        client.join(room);

        // Stranger also joined to this room
        matched.join(room);

        // Set their status to not enabled
        client.set("enabled", false);
        matched.set("enabled", false);

        // Set their rooms
        client.set("room", room);
        matched.set("room", room);

        // Notify you and stranger about connecting
        io.sockets.in(room).emit('connectedToStranger');

        // Add this conversation to database
        var conv = new Conversation({
          owner: client.id,
          stranger: matched.id,
          convID: room
        });

        // Save it!
        conv.save();
      }
    });
  }

  // City count update function
  var cityCountUpdate = function(city, amount) {
    // Update count of city
    City.findOne( {plate: city}, function(err, doc) {
      // Increase current count
      doc.count = doc.count + amount;
      doc.save();

      // Share it with other visitors.
      io.sockets.emit('setUserCountInCity', {city: city, count: doc.count});
    });
  }

  // If someone join a city, we have some job here
  client.on('userJoinedToCity', function(data) {
    matchProcess(data, true);

    City.findOne( {plate: city}, function(err, doc) {
      // Increase current count
      doc.visits = doc.visits + 1;
      doc.save();
    });
  });

  // When a client click to new chat link in client side
  client.on("newChat", function(data) {
    matchProcess(data);
  });

  client.on("goToTurkey", function(data) {
    var c = parseInt(data.old_city);

    client.leave(c);
    cityCountUpdate(c, -1);

    matchProcess({city: 0});
    cityCountUpdate(0, 1);

  });

  client.on("clientLeft", function() {
    // Get all clients in client's room
    var clients = io.sockets.clients(client.store.data.room);

    // leave this users from chat and notify them about disconnecting
    clients.forEach(function(cl) {
      cl.leave(cl.store.data.room);

      if (cl.id != client.id) {
        cl.emit("strangerDisconnected");
      }
    });
  });

  // If a new message sent notify  reciever and add this message to datatbase
  client.on("messageSent", function(data) {
    var message = data.message;

    // Notify other users in room
    client.broadcast.to(client.store.data.room).emit("newMessage", {message: message});

    // Add it to datatbase
    Conversation.findOne( {convID: client.store.data.room}, function(err, doc) {
      doc.messages.push({
        client: client.id,
        message: message
      });

      // Save boy!
      doc.save();
    });
  });

  // When client is typing notify the stranger
  client.on("clientTyping", function() {
    client.broadcast.to(client.store.data.room).emit("strangerTyping");
  });

  // When client stopped to typeing notify the stranger
  client.on("clientTypingStopped", function() {
    client.broadcast.to(client.store.data.room).emit("strangerTypingStopped");
  });

  // If someone disconnect, we should remove him.
  client.on('disconnect', function() {
    // Check if is city null, and the answer is yes, just close the door and go bro!
    if (city == null) return;

    // Get all clients in client's room
    var clients = io.sockets.clients(client.store.data.room);

    // leave this users from chat and notify them about disconnecting
    clients.forEach(function(cl) {
      cl.leave(cl.store.data.room);
      cl.emit("strangerDisconnected");
    });

    client.leave(city);

    // Update count of city
    cityCountUpdate(city, -1);

  });
});

//-------------------------------------------------------------------

/**
 * Express stuff
 */

 // When the index route requested, do your job again!
app.get('/', routes.index);

// What is Hos5 page
app.get('/nedir', page.whatishos5);

// Help Page
app.get('/yardim', page.help);

// Contact page
app.get('/iletisim', page.contact);

// Show 404 page other requests
app.use(page.notfound);

