/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

 // Load config module
var config = require("../helpers/config");

// Load file system module
var fs = require('fs');

// Load mongoose and create connection to hos5 database on given port
var mongoose = require("mongoose");
var db = mongoose.createConnection(config.config.mongodb.host, config.config.mongodb.database, config.config.mongodb.port);

// Load visitlog model
require("../models/visitlog");

console.log("Truncating of visitlog collection has been started.");

// Assign city model to a variable
var visitLog = db.model("VisitLog");

// Truncate visitLog collection
visitLog.remove({}, function(err) {
	// Check if error occured
	if (!err) {
		console.log("VisitLog collection has been truncated.");
	} else {
		console.log("An error occured while truncating the VisitLog collection.");
	}
});

