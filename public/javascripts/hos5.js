/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

// Connect to socket
$(document).ready(function() {
	if( typeof(WebSocket) != "function" ) {
		$("#browserSupport").show();
	}

	var socket = io.connect("http://hos5.net");

	// Set variables to keep city information
	var city_id = null;
	var city_name = null;

	// Title animation
	var originalTitle = document.title;
	var timeOut;

	var titleAnimation = function(msg) {
		cancelTitleAnimation();

		function loop() {
			document.title = (document.title == originalTitle) ? msg : originalTitle;

			timeOut = setTimeout(loop, 1000);
		}

		loop();
	}

	var cancelTitleAnimation = function() {
		clearTimeout(timeOut);
		document.title = originalTitle;
	}

	// If it cannot match anyone after 10 seconds, than ask user
	var turkeyTimeout;

	var askToWholeTurkey = function() {
		turkeyTimeout = setTimeout(function() {
			$("#status").html('Bu şehirde biri ile eşlemeniz uzun sürecek gibi duruyor, isterseniz <a href="#" id="gototurkey">Tüm Türkiye</a> sayfasına geçebilirsiniz.');
		}, 10000);
	}

	$("#gototurkey").live("click", function() {
		socket.emit('goToTurkey', {old_city: city_id});

		$("#status").html("Eşleştirme için kullanıcı aranıyor, lütfen bekleyiniz.");

		city_id = $("#cities a:first").attr("id");
		city_name = $("#cities a:first").data("city");

		$("#chat #logo").find("p").html(city_name);

		return false;
	});

	// Set variable for if chat is active
	var isChatActive = false;

	// If user try close the page, ask him are you sure?
	$(window).on('beforeunload', function(){
		if (isChatActive === true) {
			return 'Sohbeti terk etmek istediğinizden emin misiniz?';
		}
	});


	// Return home if logo is clicked
	$("#chat #logo").click(function() {
		window.location.reload();
	});

	// Do your job when click the city
	$("#cities").find("a").live('click', function() {
		city_id = $(this).attr('id');
		city_name = $(this).data('city');

		// Remove landing page
		$("#landing").hide('puff', {}, 500);

		// Fill the content
		setTimeout(function() {
			$("#chat #logo").find("p").html(city_name);
			$("#chat").fadeIn();
		}, 500);

		// Notify all users when a new visitor connects a city
		socket.emit('userJoinedToCity', {city: city_id});

		return false;
	});

	var originalCities = $("#cities").clone();

	$.expr[":"].contains = $.expr.createPseudo(function(arg) {
	    return function( elem ) {
	    	arg = arg.replace("i", "İ").replace("ı", "I").replace("ğ", "Ğ").replace("ü","Ü").replace("ş","Ş").replace("ö", "Ö").replace("ç","Ç");
	    	var text = $(elem).text().replace("i", "İ").replace("ı", "I").replace("ğ", "Ğ").replace("ü","Ü").replace("ş","Ş").replace("ö", "Ö").replace("ç","Ç");
	        return text.toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	    };
	});

	$("#filterCities").focus();

	$("#filterCities").keyup(function(e) {
		if ($(this).val() == "") {
			$("#cities").html(originalCities.html());
		} else {
			var cities = originalCities.find("li a:contains("+$(this).val()+")");

			var text = '';
			$.each(cities, function(index, value) {
				var id = $(this).attr("id");
				text += "<li>" + originalCities.find("li #" + id).parent().html() + "</li>";
			});

			$("#cities").html(text);

			text = '';
		}
	});

	$("#filterCities").keydown(function(e) {
	    if(e.keyCode == 13) {
	    	e.preventDefault();
	    	e.stopPropagation();
	    	if($("#filterCities").val() != "")
	        	$("#cities li a:first").click();
	        return false;
	    }
	});

	// When click the new chat link
	$("#chatAgain").live('click', function() {
		cancelTitleAnimation();

		$("#messages").empty();

		// Notify all users when a new visitor connects a city
		socket.emit('newChat', {city: city_id});

		return false;
	});

	// When chat again button clicks fire chatAgain button's action.
	$("#chatAgainBtn").click(function() {
		$("#chatAgain").click();
	});

	// Set message and notify stranger for conversation has been endedç
	$("#disconnectBtn").click(function() {
		if(confirm("Sohbeti terk etmek istediğinizden emin misiniz?")) {
			isChatActive = false;

			socket.emit("clientLeft");

			$("#status").html("Sohbet sizin tarafınızdan sonlandırıldı!");

			$("#disconnectBtn").attr('disabled', true).addClass('disabled');
			$("#type").val('').attr('disabled', true).addClass('disabled');
			$("#sendBtn").attr('disabled', true).addClass('disabled');

			$("#messages").append('<li>Başka biriyle sohbet etmek için <a href="#" id="chatAgain">tıklayınız.</a></li>');

			$("#disconnectBtn").hide();
			$("#chatAgainBtn").css('display', 'block');

			return false;
		}
	});

	// If escape has been clicked fire disconnect event
	$(document).keyup(function(e) {
	    if($("#chat").css("display") == "block" && e.keyCode == 27) {
	    	if ($("#disconnectBtn").css('display') == "block" && !$("#disconnectBtn").hasClass('disabled')) {
	    		$("#disconnectBtn").click();
	    	} else {
	    		$("#chatAgainBtn").click();
	    	}

	    }
	});

	// When send button clicks notify the stranger
	$("#sendBtn").click(function() {
		var message = $("#type");

		if (message.val().length == 0) return false;

		socket.emit("messageSent", {message: message.val()});

		$("#messages").append('<li><span class="you">Sen: </span>' + message.val() + '</li>');

		message.val('');
		message.focus();

		$("#box").scrollTop($("#messages").height());
	});

	// When enter pressed
	$("#type").keydown(function(e) {
	    if(e.keyCode == 13) {
	    	e.preventDefault();
	    	e.stopPropagation();
	        $("#sendBtn").click();
	        return false;
	    }
	});

	var typing = false;
	$("#type").keypress(function(e) {
		cancelTitleAnimation();

		if (typing === false && $(this).val() != "") {
			typing = true;
			socket.emit("clientTyping");
		}
	});

	$("#type").blur(function() {
		typing = false;
		socket.emit("clientTypingStopped");
	});

	$("#type").keyup(function() {
		if ($("#type").val() == '') {
			typing = false;
			socket.emit("clientTypingStopped");
		}
	});

	// When a new visitor comes in update count of this city
	socket.on('setUserCountInCity', function(data) {
		var city_id = data.city;
		$("#cities #" + city_id).find("span").html(data.count);
		originalCities.find("li #" + city_id + " span").html(data.count);

		var effect = false;

		$("#cities #" + city_id).stop().css({display: 'block', backgroundColor: '#333', opacity: 1}).effect("bounce", {times: 3, distance: 2});
		originalCities.find("li #" + city_id).stop().css({display: 'block', backgroundColor: '#333', opacity: 1} ).effect("pulsate", {times: 3, distance: 2});


	});

	// Notify the user for waiting a stranger to match
	socket.on('waitingForStranger', function() {
		cancelTitleAnimation();

		$("#status").html("Eşleştirme için kullanıcı aranıyor, lütfen bekleyiniz.");

		$("#disconnectBtn").attr('disabled', true).addClass('disabled');
		$("#type").text('').attr('disabled', true).addClass('disabled');
		$("#sendBtn").attr('disabled', true).addClass('disabled');

		$("#chatAgainBtn").hide();
		$("#disconnectBtn").show();

		if (city_id != 0) askToWholeTurkey();
	});

	// Notify the user that he connected with a stranger
	socket.on('connectedToStranger', function() {
		isChatActive = true;

		clearTimeout(turkeyTimeout);

		$("#status").html("Eşleşme gerçekleşti. Karşınızdakine merhaba deyin :)");

		$("#disconnectBtn").attr('disabled', false).removeClass('disabled');
		$("#type").attr('disabled', false).val('').removeClass('disabled');
		$("#sendBtn").attr('disabled', false).removeClass('disabled');

		setTimeout(function() {
			$("#type").focus();
		}, 501);
	});

	socket.on("newMessage", function(data) {
		titleAnimation("Anonim yeni bir mesaj yolladı!");

		var message = data.message;

		$("#messages #typing").remove();
		$("#messages").append('<li><span class="stranger">Anonim: </span>' + message + '</li>');
		$("#box").scrollTop($("#messages").height());
	});

	socket.on("strangerTyping", function() {
		$("#messages").append('<li id="typing">Anonim size bir ileti gönderiyor...</li>');
		$("#box").scrollTop($("#messages").height());
	});

	socket.on("strangerTypingStopped", function() {
		$("#messages #typing").remove();
	});

	// Notify the user for stranger has been disconnected.
	socket.on('strangerDisconnected', function() {
		if (isChatActive === true) {
			titleAnimation("Anonim konuşmayı terketti!");
		}

		isChatActive = false;


		$("#status").html("Sohbet ettiğiniz kişi konuşmayı terketti! Biraz daha ilgi çekici olmalısınız :(");

		$("#disconnectBtn").attr('disabled', true).addClass('disabled');
		$("#type").val('').attr('disabled', true).addClass('disabled');
		$("#sendBtn").attr('disabled', true).addClass('disabled');

		$("#messages").append('<li>Başka biriyle sohbet etmek için <a href="#" id="chatAgain">tıklayınız.</a></li>');

		$("#disconnectBtn").hide();
		$("#chatAgainBtn").css('display', 'block');
	});
});
