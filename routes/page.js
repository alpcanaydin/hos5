/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */


// Get whatishos5 page
exports.whatishos5 = function(req, res){
	res.render("whatishos5");
};

// Get help page
exports.help = function(req, res){
        res.render("help");
};

// Get contact page
exports.contact = function(req, res){
	res.render("contact");
};

// Get 404 page
exports.notfound = function(req, res, next){
	res.render("404");
};
