/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

// Load mongoose module
var mongoose = require("mongoose");

// Create conversation schema for model
var conversationSchema = new mongoose.Schema({
	// Set owner of this conversation
	owner: String,

	// Stranger that matched with owner of this conversation
	stranger: String,

	// Conversation id
	convID: String,

	// When this conversation was started?
	startedAt: { type: Date, default: Date.now },

	// Messages among them
	messages: [{
		client: String,
		message: String,
		sentAt: { type: Date, default: Date.now }
	}]
});

// Export conversation model
module.exports.conversationModel = mongoose.model('Conversation', conversationSchema);

